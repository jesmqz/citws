<?php
/* *******************************************************************
	Webservice cliente para obtener los usuarios matriculados en un curso
    de moodle
   *******************************************************************
*/
// cittest url
// $domain='https://cittest.uao.edu.co/moodledev';
// campus url
$domain='https://campus.uaovirtual.edu.co';
// token cittest moodledev
// $token='49433b66af6d7bc7d641220d65ace410';
// token campus
$token='7c3b4c59084de94a31e23d9bd263b8c9';
$function_name='core_enrol_get_enrolled_users';

$service_url=$domain. '/webservice/rest/server.php' . '?wstoken=' . $token . '&wsfunction=' . $function_name;
$restformat = '&moodlewsrestformat=json';

$args = array('courseid' => 1174);

$url_str=http_build_query($args);
print_r($args);
print_r($url_str);
$curl=curl_init($service_url . $restformat);
curl_setopt($curl, CURLOPT_POST, true);
//var_dump($args);
curl_setopt($curl, CURLOPT_POSTFIELDS, $url_str);
curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded"));
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

$curl_response = curl_exec($curl);
if ($curl_response === false) {
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('error occured during curl exec. Additioanl info: ' . var_export($info));
}
curl_close($curl);

// print_r($curl_response);
$response_object = json_decode($curl_response);
// print_r($response_object);
if ($response_object == null) {
	print("\nno tiene estudiantes\n");
	
} else {
	$users = array();
	$i=0;
	foreach($response_object as $user) {
		$users[$i] = array("id" => $user->id, "username" => $user->username, "lastaccess" => date('d-m-Y G:i',$user->lastaccess));
		// $users[$i] = array("id" => $user->id, "username" => $user->username);
		// print_r($user);
		$i++;
	}
	print_r($users);
}

?>
