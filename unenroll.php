<?php
/* *******************************************************************
	Webservice cliente para desmatricular usuario moodle
   *******************************************************************
*/
$domain='http://cittest.uao.edu.co/moodledev';
$token = '49433b66af6d7bc7d641220d65ace410';
$function_name='enrol_manual_unenrol_users';
$service_url=$domain. '/webservice/rest/server.php' . '?wstoken=' . $token . '&wsfunction=' . $function_name;
$restformat = '&moodlewsrestformat=json';


$list_students = array();
$student = array('userid' => 467,  'courseid' => 986, 'roleid' => 5);
$list_students[] = $student;

$args = array('enrolments' => $list_students);

$url_str=http_build_query($args);
$curl=curl_init($service_url . $restformat);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $url_str);
curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded"));
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

$curl_response = curl_exec($curl);
if ($curl_response === false) {
    $info = curl_getinfo($curl);
    curl_close($curl);
    die('error occured during curl exec. Additioanl info: ' . var_export($info));
}
curl_close($curl);

$response_object = json_decode($curl_response);

if (isset($response_object->exception)) {
    printf("Exception!\n");
    print_r($curl_response);
	printf("---------- \n");
	print_r($url_str);
	printf("---------- \n");
	print($service_url . $restformat);
    printf("\n");
} else {
    printf("------------------------------- \n");
    printf("enrol_manual_unenrol_users\n");
    printf("------------------------------- \n");
    printf("ARGUMENTOS \n");
    printf("---------- \n");
    print_r($args);
    printf("---------- \n");
    printf("URL ENCODED \n");
    printf("---------- \n");
    print_r($url_str);
    printf("\n");
    printf("---------- \n");
    printf("RESPONSE \n");
    printf("---------- \n");
    print_r($curl_response);
    printf("\n");
    printf("Success!\n");
}
?>
